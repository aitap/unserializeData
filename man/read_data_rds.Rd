\name{read_data_rds}
\alias{read_data_rds}
\alias{load_data}
\alias{info_rds}
\newcommand{\RDS}{\sQuote{RDS}}
\newcommand{\RData}{\sQuote{RData}}
\title{Read plain data from \RDS and \RData files}
\description{
  Read \R objects without executable code from \RDS and \RData files.
}
\usage{
  read_data_rds(file, encoding = "UTF-8")
  load_data(file, encoding = "UTF-8")
  info_rds(file)
}
\arguments{
  \item{file}{
    A single string containing the path to an \RDS file (for
    \code{read_data_rds}) or an \RData file (for \code{load_data}).

    Alternatively, a \code{\link[base]{connection}} opened in binary
    mode for reading.
  }
  \item{encoding}{
    The \dQuote{native} encoding to assume for version-2 serialization
    format.
  }
}
\details{
  Only versions 2 and 3 of the \dQuote{XDR} binary format are supported
  for now.

  Version-2 \RDS files may contain strings marked as in \dQuote{native}
  encoding, but they don't say anywhere what this encoding actually was
  at the time the file was written. Files produced on modern systems
  will most likely use \code{"UTF-8"} as the native encoding.

  If you're getting errors due to invalid byte sequences, try
  \code{"latin1"} or \code{"CP1252"}, but be aware that a wrongly
  guessed single-byte encoding will silently read the wrong characters.
}
\value{
  \item{read_data_rds}{
    The \R object from the the \RDS file.
  }
  \item{load_data}{
    The contents of the \RData file as a base \R named
    \code{\link[base]{list}}.
  }
  \item{info_rds}{
    A named list with similar structure to the one returned by
    \code{\link[base]{infoRDS}}: \describe{
      \item{\code{version}}{
        The version of the serialization format of the file, an integer
        scalar: \code{2} or \code{3}.
      }
      \item{\code{writer_version}}{
        The version of \R claiming to have written this file, a
        \code{\link[base]{numeric_version}} object.
      }
      \item{\code{min_reader_version}}{
        The minimal version of \R claimed to be able to read this file,
        \code{\link[base]{numeric_version}} object.
      }
      \item{\code{format}}{
        The variant of the serialization format used in this file.

        Currently, the \code{'xdr'} (big-endian) and \code{'binary'}
        (using the native endianness of the system producing the file)
        formats are supported.
      }
      \item{\code{native_encoding}}{
        If specified (\code{format == 3}), a character string naming the
        \dQuote{native} encoding of the \R session that is claimed to
        have written this file.
      }
      \item{\code{endian}}{
        The endianness of the binary data encoded in the file.
        \code{'big'} for \dQuote{XDR} files, guessed to be either
        \code{'little'} or \code{'big'} for \code{'binary'} files.
      }
    }
  }

  The functions will raise an error if a value of a not explicitly
  allowed type was encountered, or if one of the consistency checks
  (e.g. that UTF-8 text is actually in UTF-8) failed.
}
\seealso{
  \code{\link[base]{readRDS}}, \code{\link[base]{load}},
  \code{\link[base]{infoRDS}}
}
\examples{
  \dontshow{path <- tempfile(fileext = '.rds')}
  # plain data can be read back
  saveRDS(volcano, path)
  info_rds(path)
  stopifnot(all.equal(volcano, read_data_rds(path)))
  save(volcano, file = path)
  stopifnot(all.equal(volcano, load_data(path)$volcano))
  # executable code fails
  saveRDS(function() take_over_your_computer(), path)
  tools::assertError(read_data_rds(path), verbose = TRUE)
  \dontshow{unlink(path)}
}
\keyword{IO}
\keyword{file}
\keyword{connection}
