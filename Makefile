R = R
PACKAGE = $(shell $(R)script -e "\
 cat(read.dcf('DESCRIPTION')[,c('Package','Version')], sep = '_'); \
 cat('.tar.gz') \
")

all: $(PACKAGE)

.PHONY: all check install

$(PACKAGE): . R/* man/* man/* tests/* DESCRIPTION NAMESPACE .Rbuildignore README.md
	$(R) CMD build .

check: $(PACKAGE)
	$(R) CMD check $(PACKAGE)

install: $(PACKAGE)
	$(R) CMD INSTALL $(PACKAGE)

-include Makevars.local
publish: check
	curl --user aitap:$(CODEBERG_TOKEN) --upload-file $(PACKAGE) \
		https://codeberg.org/api/packages/aitap/cran/src
